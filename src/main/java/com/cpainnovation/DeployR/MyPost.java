package com.cpainnovation.DeployR;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.util.zip.GZIPInputStream;

import com.cpainnovation.DeployR.config.Utils;
import com.cpainnovation.DeployR.objects.Result;
import com.cpainnovation.DeployR.objects.RunProject;
import com.cpainnovation.DeployR.objects.RunProjectInputs;

public class MyPost {

	private static String cookie;
	private static String projectId;
	public static String serverName = Utils.server;
	public static String loginUrl = serverName + "/r/user/login";
	public static String executeUrl = serverName + "/r/project/execute/script";
	public static String createProjectUrl = serverName + "/r/project/create";

	public static String login(){

		try {
			
			System.out.println("Ejecutando Login...");
			
			URL obj = new URL(loginUrl);
			HttpURLConnection conn = (HttpURLConnection) obj.openConnection();
			
			setPropertiesLogin(conn);

			String outputJSON = "username="+Utils.username+"&password="+Utils.password+"&format=json";
			writeContent(conn, outputJSON);
			
			String a = printConsoleGzip(conn.getInputStream());
			getCookie(a);

		} catch (Exception ex) {
			System.out.println(ex);
		}
		
		System.out.println("Se obtubo la siguiente cookie" + cookie);
		
		return cookie;
		
	}

	public static String createProject(){

		try {
			
			System.out.println("Creando Proyecto...");
			
			URL createP = new URL(createProjectUrl);
			HttpURLConnection projectConn = (HttpURLConnection) createP.openConnection();
			
			setProperties(projectConn);
			
			String outputJSON = "format=json";
			writeContent(projectConn, outputJSON);
			
			String project = printConsoleGzip(projectConn.getInputStream());
			getProject(project);
			
			System.out.println("Proyecto creado: " + projectId);

		} catch (Exception ex) {
			System.out.println(ex);
		}
		
		return projectId;
	}

	public static String runProject(){
		
		String result = "";
		Result r = new Result();
		try {	
			
			System.out.println("Ejecutando Proyecto...");
			// Execute
			URL obj = new URL(executeUrl);
			HttpURLConnection conn = (HttpURLConnection) obj.openConnection();
			
			setPropertiesRunProject(conn);

			RunProjectInputs inputs = new RunProjectInputs("1", "01/02/2016", "01/03/2016","66245.78","22","3011.172","Union","Caja","Montevideo","Soltero","Mujer","01/01/1950","Sin Convenio","Mucama","30/12/1899","Servicios Varios","Renovacion","40000","Sin PassCard","1C");
			RunProject request = new RunProject(projectId, "true", Utils.author, Utils.filename, Utils.filedirectory, "", "", "PROBABILIDAD,CALIFICACION", inputs,"true","true");
			
			System.out.println(request.toString());
			String outputJSONex = request.toString()+"&format=json";			
			writeContent(conn, outputJSONex);
			
			result = printConsoleGzip(conn.getInputStream());
			r = Utils.parseSolution(result);
			
			System.out.println("Prob "+r.getProbabilidad());
			System.out.println("Calif "+r.getCalificacion());
			
			System.out.println("Resutlado de ejecucion: " + result);			

		} catch (Exception ex) {
			System.out.println(ex);
		}
		return "Probabilidad: "+ r.getProbabilidad()+ "  -  Calificacion: "+ r.getCalificacion();
	}

	// Aux Functiions
	private static String printConsoleGzip(InputStream is){
		
		String result = "";
		try {
			BufferedReader in = new BufferedReader ( (new InputStreamReader( new GZIPInputStream(is))));
			String inputLine;
			
			while ((inputLine = in.readLine()) != null) { 
				//System.out.println(inputLine);
				result = result + inputLine;
			}
			in.close();
		} catch (Exception ex) {
			System.out.println(ex);
		}
		
		return result;
	}
	
	private static String printConsole(InputStream is){
		
		String result = "";
		try {
			BufferedReader in = new BufferedReader ( (new InputStreamReader(is)));
			String inputLine;
			
			while ((inputLine = in.readLine()) != null) { 
				System.out.println(inputLine);
				result = result + inputLine;
			}
			in.close();
		} catch (Exception ex) {
			System.out.println(ex);
		}
		
		return result;
	}
	
	
	private static void writeContent(HttpURLConnection projectConn, String outputJSON) {
		
		try {
			byte[] outputBytes = outputJSON.getBytes("UTF-8");
			OutputStream os = projectConn.getOutputStream();
			os.write(outputBytes);
			os.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}	
	
	private static void getProject(String project) {
		
		String str = project.split("PROJECT-")[1];
		String pjrClass = str.split(",")[0];
		projectId = "PROJECT-" + pjrClass.substring(0, pjrClass.length()-1);

	}
	
	private static void getCookie(String a) {
		
		String[] str = a.split(",");
		for (int i =0 ; i< str.length ; i++ ){
			if (str[i].contains("httpcookie")){
				String cookieStr = str[i];
				String conComillas = cookieStr.split(":")[1];
				cookie = "JSESSIONID="+conComillas.substring(1, conComillas.length()-1);
				break;
			}
		}
	}

	private static void setProperties(HttpURLConnection projectConn) {
		
		try{ 
			projectConn.setRequestProperty("Accept", "*/*");
			projectConn.setRequestProperty("Accept-Encoding", "gzip, deflate");
			projectConn.setRequestProperty("Accept-Language", "es-419,es;q=0.8,en;q=0.6");
			projectConn.setRequestProperty("Connection", "keep-alive");
			projectConn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			projectConn.setRequestProperty("Host", "10.2.2.217:7400");
			projectConn.setRequestProperty("Origin", "http://10.2.2.217:7400");
			projectConn.setRequestProperty("Referer", "http://10.2.2.217:7400/repository-manager/");
			projectConn.setRequestProperty("Cache-Control","max-age=0");
			projectConn.setRequestProperty("Cookie", cookie);
			projectConn.setDoOutput(true);
			projectConn.setRequestMethod("POST");
		
		} catch (Exception ex) {
			System.out.println(ex);
		}
	}
	
	private static void setPropertiesLogin(HttpURLConnection conn) {
		
		try {
			conn.setRequestProperty("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
			conn.setRequestProperty("Accept-Encoding", "gzip, deflate");
			conn.setRequestProperty("Accept-Language", "es-419,es;q=0.8,en;q=0.6");
			conn.setRequestProperty("Cache-Control","max-age=0");
			conn.setRequestProperty("Connection", "keep-alive");
			conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			conn.setRequestProperty("Host", "10.2.2.217:7400");
			conn.setRequestProperty("Origin", "http://10.2.2.217:7400");
			conn.setRequestProperty("Referer", "http://10.2.2.217:7400/deployr/administration/login");
			conn.setRequestProperty("Upgrade-Insecure-Requests","1");
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
		
		} catch (ProtocolException e) {
			e.printStackTrace();
		}
		
	}

	private static void setPropertiesRunProject(HttpURLConnection conn) {
		
		try {
			conn.setRequestProperty("Accept", "*/*");
			conn.setRequestProperty("Accept-Encoding", "gzip, deflate");
			conn.setRequestProperty("Accept-Language", "es-419,es;q=0.8,en;q=0.6");
			conn.setRequestProperty("Connection", "keep-alive");
			conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			conn.setRequestProperty("Host", "10.2.2.217:7400");
			conn.setRequestProperty("Origin", "http://10.2.2.217:7400");
			conn.setRequestProperty("Referer", "http://10.2.2.217:7400/repository-manager/");
			conn.setRequestProperty("Cache-Control","max-age=0");
			conn.setRequestProperty("Cookie",cookie);
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			
		} catch (ProtocolException e) {
			e.printStackTrace();
		}
		
	}
	
}
