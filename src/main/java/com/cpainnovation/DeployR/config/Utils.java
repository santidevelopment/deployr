package com.cpainnovation.DeployR.config;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import com.cpainnovation.DeployR.objects.Result;
import com.cpainnovation.DeployR.objects.Value;
import com.google.gwt.thirdparty.json.JSONException;
import com.google.gwt.thirdparty.json.JSONObject;

public class Utils {

	public static String username = "";
	public static String password = "";
	public static String server = "";
	public static String filename = "";
	public static String filedirectory = "";
	public static String author = "";
	
	public static void loadProperties(){
		
		Properties prop = new Properties();
		InputStream input = null;

		try {
			input = Utils.class.getResourceAsStream("deploy.properties");

			// load a properties file
			prop.load(input);

			// get the property value and print it out
			System.out.println(prop.getProperty("user"));
			System.out.println(prop.getProperty("password"));
			System.out.println(prop.getProperty("url"));
			System.out.println(prop.getProperty("port"));
			
			username = prop.getProperty("user");
			password = prop.getProperty("password");
			server = "http://" + prop.getProperty("url") + ":" + prop.getProperty("port") + "/deployr"; 
			filename=prop.getProperty("filename");
			filedirectory=prop.getProperty("filedirectory");
			author=prop.getProperty("author");
	
		} catch (IOException io) {
			io.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
	
		}
	}


	public static Result parseSolution(String result){
		Result sol = new Result();
		
		try {
			JSONObject json = new JSONObject(result);
			JSONObject probJson = (JSONObject) json.getJSONObject("deployr").getJSONObject("response").getJSONObject("workspace").getJSONArray("objects").get(0);
			JSONObject califJson = (JSONObject) json.getJSONObject("deployr").getJSONObject("response").getJSONObject("workspace").getJSONArray("objects").get(1);
			
			
			sol.setProbabilidad(probJson.getString("value"));
			sol.setCalificacion((String)califJson.getJSONArray("value").get(0));
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return sol;
	}
}
