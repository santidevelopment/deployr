package com.cpainnovation.DeployR;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.servlet.annotation.WebServlet;

import com.cpainnovation.DeployR.config.Utils;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.annotations.Widgetset;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;


@Theme("mytheme")
@Widgetset("com.cpainnovation.DeployR.DeployRWidgetset")
public class DeployRUI extends UI {

	private static final long serialVersionUID = 1L;
	@Override
    protected void init(VaadinRequest vaadinRequest) {
     
		Utils.loadProperties();
    	final VerticalLayout layout = new VerticalLayout();
        layout.setMargin(true);
        setContent(layout);
        
        // LOGIN
	    Button login = new Button("Login");
	    login.addClickListener(new Button.ClickListener() {
	        
			private static final long serialVersionUID = 1L;

			@Override
	        public void buttonClick(ClickEvent event) {
	        	
	            String cookie = MyPost.login();
	            layout.addComponent(new Label("Login Realizado. Cookie obtenida: " + cookie));
	            
	        }
	    });
	    layout.addComponent(login);
	    
        // Create Project
        Button testSanti = new Button("Crear Proyecto");
        testSanti.addClickListener(new Button.ClickListener() {
            
			private static final long serialVersionUID = 1L;

			@Override
            public void buttonClick(ClickEvent event) {
            	
                String proyecto = MyPost.createProject();
                layout.addComponent(new Label("Proyecto Creado. Id de Proyecto: " + proyecto));
                
            }
        });
        layout.addComponent(testSanti);
        
        // Execute Project
        Button runProjectBtn = new Button("Ejecutar");
        runProjectBtn.addClickListener(new Button.ClickListener() {
            
			private static final long serialVersionUID = 1L;

			@Override
            public void buttonClick(ClickEvent event) {
				
				SimpleDateFormat format = new SimpleDateFormat("E, dd MMM yyyy HH:mm:ss z");
				
				layout.addComponent(new Label(format.format(Calendar.getInstance().getTime())));
                String resultado = MyPost.runProject();
                layout.addComponent(new Label("Resultado: " + resultado));
                
                layout.addComponent(new Label(format.format(Calendar.getInstance().getTime())));
            	
                
            }
        });
        layout.addComponent(runProjectBtn);

	    

    }
    @WebServlet(urlPatterns = "/*", name = "DeployRUIServlet", asyncSupported = true)
    @VaadinServletConfiguration(ui = DeployRUI.class, productionMode = false)
    public static class DeployRUIServlet extends VaadinServlet {


		private static final long serialVersionUID = 1L;
    }
}
