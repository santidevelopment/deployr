package com.cpainnovation.DeployR.objects;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class RunProjectInputs implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private Value IdCliente;
	private Value FecOpe;
	private Value PriCuota;
	private Value MontoTotal;
	private Value Cuotas;
	private Value MontoCuota;
	private Value Sucursal;
	private Value FormaPago;
	private Value Departamento;
	private Value estadoCivil;
	private Value Sexo;
	private Value fechaNacimiento;
	private Value EmpConvenio;
	private Value cargo;
	private Value fechaIngreso;
	private Value Actividad;
	private Value PrimerCre;
	private Value CapitalPrestado;
	private Value EstadoCtaPC;
	private Value CalificacionBCU;
	
	public RunProjectInputs(){
		
	}
	
	public RunProjectInputs(String idCliente, String fecOpe, String priCuota, String montoTotal, String cuotas,
			String montoCuota, String sucursal, String formaPago, String departamento, String estadoCivil, String sexo,
			String fechaNacimiento, String empConvenio, String cargo, String fechaIngreso, String actividad, String primerCre,
			String capitalPrestado, String estadoCtaPC, String calificacionBCU) {
		super();
		this.IdCliente = new Value(idCliente);
		this.FecOpe = new Value("\""+fecOpe+"\"");
		this.PriCuota = new Value("\""+priCuota+"\"");
		this.MontoTotal = new Value(montoTotal);
		this.Cuotas = new Value(cuotas);
		this.MontoCuota = new Value(montoCuota);
		this.Sucursal = new Value("\""+sucursal+"\"");
		this.FormaPago = new Value("\""+formaPago+"\"");
		this.Departamento = new Value("\""+departamento+"\"");
		this.estadoCivil = new Value("\""+estadoCivil+"\"");
		this.Sexo = new Value("\""+sexo+"\"");
		this.fechaNacimiento = new Value("\""+fechaNacimiento+"\"");
		this.EmpConvenio = new Value("\""+empConvenio+"\"");
		this.cargo = new Value("\""+cargo+"\"");
		this.fechaIngreso = new Value("\""+fechaIngreso+"\"");
		this.Actividad = new Value("\""+actividad+"\"");
		this.PrimerCre = new Value("\""+primerCre+"\"");
		this.CapitalPrestado = new Value(capitalPrestado);
		this.EstadoCtaPC = new Value("\""+estadoCtaPC+"\"");
		this.CalificacionBCU = new Value("\""+calificacionBCU+"\"");
	}

	
	public String toString(){
		String a = "{" +
				"\"IdCliente\":"+IdCliente.toString()+","+
				"\"FecOpe\":"+FecOpe.toString()+","+
				"\"PriCuota\":"+PriCuota.toString()+","+
				"\"MontoTotal\":"+MontoTotal.toString()+","+
				"\"Cuotas\":"+Cuotas.toString()+","+
				"\"MontoCuota\":"+MontoCuota.toString()+","+
				"\"Sucursal\":"+Sucursal.toString()+","+
				"\"FormaPago\":"+FormaPago.toString()+","+
				"\"Departamento\":"+Departamento.toString()+","+
				"\"estadoCivil\":"+estadoCivil.toString()+","+
				"\"Sexo\":"+Sexo.toString()+","+
				"\"fechaNacimiento\":"+fechaNacimiento.toString()+","+
				"\"EmpConvenio\":"+EmpConvenio.toString()+","+
				"\"cargo\":"+cargo.toString()+","+
				"\"fechaIngreso\":"+fechaIngreso.toString()+","+
				"\"Actividad\":"+Actividad.toString()+","+
				"\"PrimerCre\":"+PrimerCre.toString()+","+
				"\"CapitalPrestado\":"+CapitalPrestado.toString()+","+
				"\"EstadoCtaPC\":"+EstadoCtaPC.toString()+","+
				"\"CalificacionBCU\":"+CalificacionBCU.toString()+"}";
		
		try {
			 a = URLEncoder.encode(a,"UTF-8");
		} catch (UnsupportedEncodingException e) {
			a= "";
			e.printStackTrace();
		}
		return a;
	}
	
	public Value getIdCliente() {
		return IdCliente;
	}
	public void setIdCliente(Value idCliente) {
		IdCliente = idCliente;
	}
	public Value getFecOpe() {
		return FecOpe;
	}
	public void setFecOpe(Value fecOpe) {
		FecOpe = fecOpe;
	}
	public Value getPriCuota() {
		return PriCuota;
	}
	public void setPriCuota(Value priCuota) {
		PriCuota = priCuota;
	}
	public Value getMontoTotal() {
		return MontoTotal;
	}
	public void setMontoTotal(Value montoTotal) {
		MontoTotal = montoTotal;
	}
	public Value getCuotas() {
		return Cuotas;
	}
	public void setCuotas(Value cuotas) {
		Cuotas = cuotas;
	}
	public Value getMontoCuota() {
		return MontoCuota;
	}
	public void setMontoCuota(Value montoCuota) {
		MontoCuota = montoCuota;
	}
	public Value getSucursal() {
		return Sucursal;
	}
	public void setSucursal(Value sucursal) {
		Sucursal = sucursal;
	}
	public Value getFormaPago() {
		return FormaPago;
	}
	public void setFormaPago(Value formaPago) {
		FormaPago = formaPago;
	}
	public Value getDepartamento() {
		return Departamento;
	}
	public void setDepartamento(Value departamento) {
		Departamento = departamento;
	}
	public Value getEstadoCivil() {
		return estadoCivil;
	}
	public void setEstadoCivil(Value estadoCivil) {
		this.estadoCivil = estadoCivil;
	}
	public Value getSexo() {
		return Sexo;
	}
	public void setSexo(Value sexo) {
		Sexo = sexo;
	}
	public Value getFechaNacimiento() {
		return fechaNacimiento;
	}
	public void setFechaNacimiento(Value fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	public Value getEmpConvenio() {
		return EmpConvenio;
	}
	public void setEmpConvenio(Value empConvenio) {
		EmpConvenio = empConvenio;
	}
	public Value getCargo() {
		return cargo;
	}
	public void setCargo(Value cargo) {
		this.cargo = cargo;
	}
	public Value getFechaIngreso() {
		return fechaIngreso;
	}
	public void setFechaIngreso(Value fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}
	public Value getActividad() {
		return Actividad;
	}
	public void setActividad(Value actividad) {
		Actividad = actividad;
	}
	public Value getPrimerCre() {
		return PrimerCre;
	}
	public void setPrimerCre(Value primerCre) {
		PrimerCre = primerCre;
	}
	public Value getCapitalPrestado() {
		return CapitalPrestado;
	}
	public void setCapitalPrestado(Value capitalPrestado) {
		CapitalPrestado = capitalPrestado;
	}
	public Value getEstadoCtaPC() {
		return EstadoCtaPC;
	}
	public void setEstadoCtaPC(Value estadoCtaPC) {
		EstadoCtaPC = estadoCtaPC;
	}
	public Value getCalificacionBCU() {
		return CalificacionBCU;
	}
	public void setCalificacionBCU(Value calificacionBCU) {
		CalificacionBCU = calificacionBCU;
	}
	
	

}
