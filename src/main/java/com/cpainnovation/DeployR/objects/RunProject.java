package com.cpainnovation.DeployR.objects;

import java.io.Serializable;

public class RunProject implements Serializable {

	private static final long serialVersionUID = 1L;
	private String project;
	private String enableConsoleEvents;
	private String author;
	private String filename;
	private String directory;
	private String version;
	private String csvinputs;
	private String robjects;
	private RunProjectInputs inputs;
	private String echooff;
	private String consoleoff;
	

	public RunProject(String project, String enableConsoleEvents, String author, String filename, String directory,
			String version, String csvinputs, String robjects, RunProjectInputs inputs, String echooff, String consoleoff) {
		super();
		this.project = project;
		this.enableConsoleEvents = enableConsoleEvents;
		this.author = author;
		this.filename = filename;
		this.directory = directory;
		this.version = version;
		this.csvinputs = csvinputs;
		this.robjects = robjects;
		this.inputs = inputs;
		this.echooff = echooff;
		this.consoleoff = consoleoff;
	}


	public String toString(){
		String a = 
				"project="+project+"&"+
				"enableConsoleEvents="+enableConsoleEvents+"&"+
				"author="+author+"&"+
				"filename="+filename+"&"+
				"directory="+directory+"&"+
				"version="+version+"&"+
				"csvinputs="+csvinputs+"&"+
				"robjects="+robjects+"&"+
				"inputs="+inputs.toString()+"&"+
				"echooff="+echooff+"&"+
				"consoleoff="+consoleoff+"";
		return a;
	}
	
	public String getProject() {
		return project;
	}
	public void setProject(String project) {
		this.project = project;
	}
	public String getEnableConsoleEvents() {
		return enableConsoleEvents;
	}
	public void setEnableConsoleEvents(String enableConsoleEvents) {
		this.enableConsoleEvents = enableConsoleEvents;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public String getDirectory() {
		return directory;
	}
	public void setDirectory(String directory) {
		this.directory = directory;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getCsvinputs() {
		return csvinputs;
	}
	public void setCsvinputs(String csvinputs) {
		this.csvinputs = csvinputs;
	}
	public String getRobjects() {
		return robjects;
	}
	public void setRobjects(String robjects) {
		this.robjects = robjects;
	}
	public RunProjectInputs getInputs() {
		return inputs;
	}
	public void setInputs(RunProjectInputs inputs) {
		this.inputs = inputs;
	}


	public String getEchooff() {
		return echooff;
	}


	public void setEchooff(String echooff) {
		this.echooff = echooff;
	}


	public String getConsoleoff() {
		return consoleoff;
	}


	public void setConsoleoff(String consoleoff) {
		this.consoleoff = consoleoff;
	}
}
