package com.cpainnovation.DeployR.objects;

import java.io.Serializable;

public class Value implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private String type;
	private String value;

	public Value() {
	}
	public Value(String type, String value) {
		this.value = value;
		this.type = type;
	}
	public Value(String value) {
		this.type = "primitive";
		this.value = value;
	}

	public String toString(){
		return "{\"type\":\""+type+"\","+
				"\"value\":"+value+"}";
	}
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}

}
