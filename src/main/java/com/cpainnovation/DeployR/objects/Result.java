package com.cpainnovation.DeployR.objects;

public class Result {

	private String probabilidad;
	private String calificacion;

	public String getProbabilidad() {
		return probabilidad;
	}
	public void setProbabilidad(String probabilidad) {
		this.probabilidad = probabilidad;
	}
	
	public String getCalificacion() {
		return calificacion;
	}
	public void setCalificacion(String calificacion) {
		this.calificacion = calificacion;
	}	
	
}
